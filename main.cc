#include "htaskset.h"
#include "hrootsource.h"
#include "hparticletrackcleaner.h"
#include "hparticletracksorter.h"
#include "hparasciifileio.h"
#include <getopt.h>
#include <iostream>
#include "hntuple.h"
#include "htrackplayer.h"
#include "htrackcut.h"
#include "htimecut.h"
#include "hgraphcut.h"
#include "hdedxcut.h"
#include "hparticleplayer.h"
#include "hhypplayer.h"
#include "hparticlepool.h"
#include "hhyppool.h"
#include "hpidpool.h"
#include "houtputfile.h"
#include "houtput.h"
#include "heditor.h"
#include "hphysicsconstants.h"
#include "henergylosscorrpar.h"
#include <TF1.h>
//#include "hcommondef.h"


using namespace std;
//using namespace CommonDefinitions;

Bool_t myselectlepton(HParticleCand* pcand); // track cleaner methods
Bool_t myselecthadron(HParticleCand* pcand); // track cleaner methods

/*********************************************************************************/
/* Here set whether you run:                                                     */
/* experiment (simflag = 0)                                                      */ 
/* simulation (simflag = 1)                                                      */
/* embedded mode (simflag = 2)                                                   */
/*                                                                               */
/* here set whether you have also Forward Wall data (HWallHit or HWallHitSim)    */
/*********************************************************************************/

Int_t simflag = 1;
Int_t wallflag = 1;
Int_t startflag = 0;
Int_t piontrackerflag = 0;

#define LEPTONS 1
//#define HADRONS 1

/*********************************************************************************/
/*********************************************************************************/

int main(Int_t argc, Char_t **argv)
{
    HEnergyLossCorrPar enLossCorr;
    //enLossCorr.setDefaultPar("jul14_PE"); // "jul14_W" - Wolfram target, "jul14_C3" - Carbon 3 segments, "jul14_PE" & "aug14_PE" - PE target, "aug14_C7" - Carbon 7 segments
    //enLossCorr.setDefaultPar("jul14_C3"); // "jul14_W" - Wolfram target, "jul14_C3" - Carbon 3 segments, "jul14_PE" & "aug14_PE" - PE target, "aug14_C7" - Carbon 7 segments
    enLossCorr.setDefaultPar("aug14_PE"); // "jul14_W" - Wolfram target, "jul14_C3" - Carbon 3 segments, "jul14_PE" & "aug14_PE" - PE target, "aug14_C7" - Carbon 7 segments
    //enLossCorr.setDefaultPar("aug14_C7"); // "jul14_W" - Wolfram target, "jul14_C3" - Carbon 3 segments, "jul14_PE" & "aug14_PE" - PE target, "aug14_C7" - Carbon 7 segments

  TString outDir=""; // DO NOT CHANGE IT - HYDRA PARAM
  TString outFile=""; // DO NOT CHANGE IT - HYDRA PARAM

  //***----------------------------------------------------------
  // - - - - sim TString inputDir  ="/hera/hades/user/przygoda/PAT2/";
  //************ federico ****************
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/1pi0_new/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/Delta0_carbon_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/Delta0_dalitz_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/Delta0_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/Delta0_n_carbon_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/Delta0_n_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/Delta1_carbon_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/Delta1_dalitz_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/Delta1_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/eta_carbon_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/eta_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/N1520_carbon_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/N1520_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/N1535_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/pi0n_carbon_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/pi0n_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/rho_carbon_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/fscozzi/simulation_2019/DST_output/rho_large_stat/";
  //TString inputDir  ="/lustre/nyx/hades/user/nrathod/PIMP_simul/DST/outputs/brake_rad_dst/root/";
  TString inputDir  ="/lustre/nyx/hades/user/nrathod/PIMP_simul/DST/outputs/PWA_events_dst/root/";
  ////******************************************************
  ////******************************************************
  ////******************************************************
  ////******************************************************
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/N/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PS/root15/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PS/root19/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PS/root17/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PS/root18/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PS/root20/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PS/root25/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PS/root30/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/N/656/root/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/N/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/N/748/root/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/N/800/root/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/656/root/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/748/root/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/800/root/";
  ////******************************************************
  ////******************************************************
  ////******************************************************
  ////******************************************************
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/800/root20/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/800/root21/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/800/root22/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/800/root23/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/800/root24/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/800/root25/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/800/root26/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/800/root27/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/800/root28/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/800/root29/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/PI0/800/root30/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/EL/800/root20/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/EL/800/root21/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/EL/800/root22/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/EL/800/root23/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/EL/800/root24/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/EL/800/root25/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/EL/800/root26/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/EL/800/root27/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/EL/800/root28/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/EL/800/root29/";
  //TString inputDir  ="/lustre/nyx/hades/user/przygoda/PION/DST/FILES/EL/800/root30/";
  ////******************************************************
  ////******************************************************
  ////******************************************************
  ////******************************************************
  //TString inputDir  ="/hera/hades/user/przygoda/PION/DST/690_dat/out/";
  //TString inputDir  ="/hera/hades/user/przygoda/PION/DST/748_dat/out/";
  //TString inputDir  ="/hera/hades/user/przygoda/PION/DST/800_dat/out/";
  //TString inputDir  ="/hera/hades/dst/jul14/gen2/195/root/";
  //TString inputDir  ="/hera/hades/dst/jul14/gen2/196/root/";
  //TString inputDir  ="/hera/hades/dst/jul14/gen2/197/root/";
  //TString inputDir  ="/hera/hades/dst/jul14/gen2/198/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/232/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/233/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/234/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/235/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/236/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/237/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/238/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/239/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/240/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/241/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/242/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/243/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/244/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/245/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/246/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/249/612/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/250/612/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/250/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/251/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/254/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/255/690/root/";
  //TString inputDir  ="/lustre/nyx/hades/dst/aug14/gen2/256/690/root/";

  TString inputFile = argv[1];

  //**************** federico *****************
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/1pi0_new/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/Delta0_carbon_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/Delta0_dalitz_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/Delta0_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/Delta0_n_carbon_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/Delta0_n_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/Delta1_carbon_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/Delta1_dalitz_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/Delta1_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/eta_carbon_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/eta_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/N1520_carbon_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/N1520_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/N1535_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/pi0n_carbon_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/pi0n_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/rho_carbon_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/rho_large_stat/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/bremss/";
  TString output_Dir ="/lustre/nyx/hades/user/przygoda/PATBT/out/sim/LEPTONS/PWApi0/";
  //*******************************************
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/elastic/GEN2_NEW/";
  //*******************************************
  //*******************************************
  //*******************************************
  //*******************************************
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/N/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F19/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F17/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F18/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F20/";
  //---
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F20NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F21NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F22NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F23NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F24NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F25NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F26NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F27NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F28NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F29NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F30NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/EL/F20NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/EL/F21NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/EL/F22NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/EL/F23NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/EL/F24NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/EL/F25NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/EL/F26NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/EL/F27NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/EL/F28NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/EL/F29NEW/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/EL/F30NEW/";
  //---
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PWA_PS_PI0/F30/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/N/690/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/N/748/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/N/800/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PI0/656/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PI0/690/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PI0/748/";
  //TString output_Dir ="/lustre/nyx/hades/user/przygoda/PAT2/out/sim/PI0/800/";
  //*******************************************
  //*******************************************
  //*******************************************
  //*******************************************
  //TString output_Dir ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/234test/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/195test/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/195/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/LEPTONS/195/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/196/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/196/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/197/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/197/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/198/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/198/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/232/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/232/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/233/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/233/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/234/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/234/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/235/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/235/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/236/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/236/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/237/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/237/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/238/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/238/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/239/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/239/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/240/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/240/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/241/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/241/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/242/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/242/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/243/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/243/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/244/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/244/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/245/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/245/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/246/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/246/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/246/748/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/246/748/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/247/656/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/247/656/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/247/748/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/247/748/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/248/656/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/248/656/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PAT2/out/exp/gen2/249/612/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/249/656/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/249/656/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/249/800/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/249/800/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PAT2/out/exp/gen2/250/612/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/250/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/250/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/250/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/251/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/251/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/251/800/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/251/800/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/252/656/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/252/656/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/252/748/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/252/748/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/252/800/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/252/800/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/253/656/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/253/656/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/254/656/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen1/LEPTONS/254/656/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/254/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/254/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/255/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/255/";
  //TString output_Dir  ="/hera/hades/user/przygoda/PAT2/out/exp/gen2/256/";
  //TString output_Dir  ="/lustre/nyx/hades/user/przygoda/PATBT/out/exp/gen2/LEPTONS/256/";

  //TString output_Dir   = "/tmp/";
  TString output_File  = inputFile;
  TString output_File2  = inputFile;
          output_File.ReplaceAll(".root", "_out.root");
          output_File2.ReplaceAll(".root", "_hadron_out.root");


  TStopwatch timer;
  Int_t evN=0;
  // simflag is now a global variable, please look above the int main() 
  Int_t /*simflag=1,*/ nRunId=0, nEvents=0, startEvt=0;

  // set gHades
  if(gHades == NULL) new Hades;
  gHades->setTreeBufferSize(8000);

  const char *context;
  if (simflag==1) context="simulation";
  else context="real";

  ErrorMsg(INFO, "main", 2, "Context is ",context);

        HParticleTrackCleaner* cleaner = new HParticleTrackCleaner();
        //HParticleTrackSorter& sorter = cleaner->getSorter();
        //sorter.resetFlags(kTRUE,kTRUE,kTRUE,kTRUE);
        //sorter.fill(HParticleTrackSorter::selectLeptons);
        //sorter.selectBest(HParticleTrackSorter::kIsBestRK,HParticleTrackSorter::kIsLepton);
        HParticleTrackSorter::setBetaLeptonCut(0.5);
        //sorter.fill(HParticleTrackSorter::selectHadrons);
        //sorter.selectBest(HParticleTrackSorter::kIsBestRK,HParticleTrackSorter::kIsHadron);
        //sorter.init();  
        // HParticleTrackSorter::setIgnoreRICH();
        // HParticleTrackSorter::setIgnoreInnerMDC();
        //cleaner->setUserSelectionLeptons(myselectlepton);
        //cleaner->setUserSelectionHadrons(myselecthadron);
        gHades->getTaskSet(context)->add(cleaner);

  //*** PostDST Analysis Tool - PAT ***

#ifdef LEPTONS
  HOutputFile outputFile( (output_Dir+output_File).Data(), "recreate" );
#endif
#ifdef HADRONS
  HOutputFile outputFile2( (output_Dir+output_File2).Data(), "recreate" );
#endif

  HParticlePool myParticles;
//  HParticlePool myParticles( &outputFile );
  myParticles.add("all",eHadronPos,eHadronNeg,eLeptonPos,eLeptonNeg);

  HHypPool myHyps;
  //HHypPool myHyps( &outputFile );
  //HHypPool myHyps( &outputFile2 );
#ifdef LEPTONS
  myHyps.add("Lp",eLeptonPos);
  myHyps.add("Lm",eLeptonNeg);
  //***------------------------------------------------
  myHyps.add("LpLm",eLeptonPos,eLeptonNeg);
  myHyps.add("LpLp",eLeptonPos,eLeptonPos);
  myHyps.add("LmLm",eLeptonNeg,eLeptonNeg);
  myHyps.add("HmLpLm", eHadronNeg,eLeptonPos,eLeptonNeg);
  myHyps.add("HmLpLp", eHadronNeg,eLeptonPos,eLeptonPos);
  myHyps.add("HmLmLm", eHadronNeg,eLeptonNeg,eLeptonNeg);
  myHyps.add("HpHmLpLm", eHadronPos,eHadronNeg,eLeptonPos,eLeptonNeg);
  myHyps.add("HpHmLpLp", eHadronPos,eHadronNeg,eLeptonPos,eLeptonPos);
  myHyps.add("HpHmLmLm", eHadronPos,eHadronNeg,eLeptonNeg,eLeptonNeg);
  //myHyps.add("LpLmLpLm",eLeptonPos,eLeptonNeg,eLeptonPos,eLeptonNeg);
#endif 
  //***------- hadron stuff ---------------------------
#ifdef HADRONS
  //myHyps.add("Hp", eHadronPos);
  //myHyps.add("Hm", eHadronNeg);
  //myHyps.add("HpHp", eHadronPos,eHadronPos);
  myHyps.add("HpHm", eHadronPos,eHadronNeg);
  myHyps.add("HpHmHpHm", eHadronPos,eHadronNeg,eHadronPos,eHadronNeg);
  myHyps.add("HpHmLpLm", eHadronPos,eHadronNeg,eLeptonPos,eLeptonNeg);
#endif
  //*************************************************** 
#ifdef LEPTONS 
  //HPidPool myPids( &outputFile );
  HPidPool myPids;
#endif
#ifdef HADRONS
  //HPidPool myPids2( &outputFile2 );
  HPidPool myPids2;
#endif
  //***------------------------------------------------
#ifdef LEPTONS 
  //***------------------------------------------------
  myPids.add("LpLm", "EpEm",ePositron,eElectron);
  myPids.add("LpLp", "EpEp",ePositron,ePositron);
  myPids.add("LmLm", "EmEm",eElectron,eElectron);
  myPids.add("HmLpLm", "PimEpEm",ePiMinus,ePositron,eElectron);
  myPids.add("HmLpLp", "PimEpEp",ePiMinus,ePositron,ePositron);
  myPids.add("HmLmLm", "PimEmEm",ePiMinus,eElectron,eElectron);
  myPids.add("HpHmLpLm", "PPimEpEm",eProton,ePiMinus,ePositron,eElectron);
  myPids.add("HpHmLpLp", "PPimEpEp",eProton,ePiMinus,ePositron,ePositron);
  myPids.add("HpHmLmLm", "PPimEmEm",eProton,ePiMinus,eElectron,eElectron);
  //myPids.add("LpLmLpLm", "EpEmEpEm",ePositron,eElectron,ePositron,eElectron);
#endif 
  //***------- hadron stuff ---------------------------
#ifdef HADRONS
  //myPids2.add("HpHp", "PP",eProton,eProton);
  //myPids2.add("HpHp", "PPip",eProton,ePiPlus);
  myPids2.add("HpHm", "PipPim",ePiPlus,ePiMinus);
  myPids2.add("HpHm", "PPim",eProton,ePiMinus);
  myPids2.add("HpHmHpHm", "PPimPipPim",eProton,ePiMinus,ePiPlus,ePiMinus);
  myPids2.add("HpHmLpLm", "PipPimEpEm",ePiPlus,ePiMinus,ePositron,eElectron);
#endif
  //*************************************************** 
#ifdef LEPTONS 
  HPidPool myPids_A( &outputFile );
#endif
#ifdef HADRONS
  HPidPool myPids_A2( &outputFile2 );
  //HPidPool myPids_A2;
#endif
  //***------------------------------------------------
#ifdef LEPTONS 
  //***------------------------------------------------
  myPids_A.add("LpLm", "EpEm_ID",ePositron,eElectron);
  myPids_A.add("LpLp", "EpEp_ID",ePositron,ePositron);
  myPids_A.add("LmLm", "EmEm_ID",eElectron,eElectron);
  myPids_A.add("HmLpLm", "PimEpEm_ID",ePiMinus,ePositron,eElectron);
  myPids_A.add("HmLpLp", "PimEpEp_ID",ePiMinus,ePositron,ePositron);
  myPids_A.add("HmLmLm", "PimEmEm_ID",ePiMinus,eElectron,eElectron);
  myPids_A.add("HpHmLpLm", "PPimEpEm_ID",eProton,ePiMinus,ePositron,eElectron);
  myPids_A.add("HpHmLpLp", "PPimEpEp_ID",eProton,ePiMinus,ePositron,ePositron);
  myPids_A.add("HpHmLmLm", "PPimEmEm_ID",eProton,ePiMinus,eElectron,eElectron);
  //myPids_A.add("LpLmLpLm", "EpEmEpEm_ID",ePositron,eElectron,ePositron,eElectron);
#endif 
  //***------- hadron stuff ---------------------------
#ifdef HADRONS
  //myPids_A2.add("HpHp", "PP_ID",eProton,eProton);
  //myPids_A2.add("HpHp", "PPip_ID",eProton,ePiPlus);
  myPids_A2.add("HpHm", "PipPim_ID",ePiPlus,ePiMinus);
  myPids_A2.add("HpHm", "PPim_ID",eProton,ePiMinus);
  myPids_A2.add("HpHmHpHm", "PPimPipPim_ID",eProton,ePiMinus,ePiPlus,ePiMinus);
  myPids_A2.add("HpHmLpLm", "PipPimEpEm_ID",ePiPlus,ePiMinus,ePositron,eElectron);
#endif

  HTrackCut tCut("all");
  HTimeCut tCut2("all");
  HGraphCut tCut3("all","/lustre/nyx/hades/user/przygoda/PATBT/PION_CUTS_gen0b.root");
  //HGraphCut tCut3("all","/u/przygoda/PAT2/DP_PID_cuts.root");
  //HDedxCut tCut4("all","M3_DEDXCUTS_PAT.root");

  HTrackPlayer * hyp = new HTrackPlayer( myParticles );
  HParticlePlayer * hyp2 = new HParticlePlayer(myParticles, myHyps);
#ifdef LEPTONS 
  HHypPlayer * hyp3 = new HHypPlayer(myHyps, myPids);
  HHypPlayer * hyp3_A = new HHypPlayer(myHyps, myPids_A);
#endif
#ifdef HADRONS 
  HHypPlayer * hyp3H = new HHypPlayer(myHyps, myPids2);
  HHypPlayer * hyp3_AH = new HHypPlayer(myHyps, myPids_A2);
#endif

  //hyp->add( tCut );

#ifdef LEPTONS 
  hyp3->add( tCut2 );
#endif
#ifdef HADRONS 
  hyp3H->add( tCut2 );
#endif

#ifdef LEPTONS 
  hyp3_A->add( tCut2 );
  hyp3_A->add( tCut3 );
#endif
#ifdef HADRONS 
  hyp3_AH->add( tCut2 );
  hyp3_AH->add( tCut3 );
#endif

  //Set batch (needed for TCanvas's)

  gROOT->SetBatch();

  //Add input files
  HRootSource *source=new HRootSource;
  source->setDirectory((Text_t*)inputDir.Data());
  source->addFile((Text_t*)inputFile.Data());

  gHades->setDataSource(source);
  if (nRunId) {
    source->setGlobalRefId(nRunId);
    // source->setRefId(nRunId,nRunId);// This might be better
  }

  
  //HRuntimeDb* rtdb=gHades->getRuntimeDb();

  gHades->getTaskSet(context)->add(hyp);
  gHades->getTaskSet(context)->add(hyp2);
#ifdef LEPTONS
  gHades->getTaskSet(context)->add(hyp3);
  gHades->getTaskSet(context)->add(hyp3_A);
#endif
#ifdef HADRONS
  gHades->getTaskSet(context)->add(hyp3H);
  gHades->getTaskSet(context)->add(hyp3_AH);
#endif


  gHades->getTaskSet(context)->print();

  //------------------------ Initialization ----------------------------
  cout<<"gHades->init()\n";

  gHades->makeCounter(1000);
  if(!gHades->init())
    cerr<<"Error gHades->init() returns false\n";


  //Set output

  if (! (outDir.EndsWith("null") || outDir.EndsWith("none") || outDir=="")) {
    gHades->setOutputFile((Text_t*)outFile.Data(),"RECREATE",const_cast<Text_t*>("Test"),2);
    gHades->makeTree();
  }

  //--------------------------------------------------------------------
        // gHades->printDefinedTaskSets();
        // gHades->setQuietMode(0);
        //nEvents = 10000;
  cout<<"Processing events...\n";
  timer.Reset();
  timer.Start();
  if ((nEvents<1) && (startEvt == 0) ) {
    evN=gHades->eventLoop();
  } else {
    evN=gHades->eventLoop(nEvents,startEvt);
  }

  gHades->getTaskSet(context)->printTimer();

  printf("rtdb deleted\n");
  delete gHades;

  timer.Stop();

  cout<<"------------------------------------------------------\n";
  cout<<"Events processed: "<<evN<<endl;
  cout<<"Real time: "<<timer.RealTime()<<endl;;
  cout<<"Cpu time: "<<timer.CpuTime()<<endl;
  if (evN) cout<<"Performance: "<<timer.CpuTime()/evN<<endl;;

  return 0;
}// END Int_t fill(TString, Int_t , Int_t)



// **************************** BELOW OBSOLETE FUNCTIONS *************************************
// **************************** BELOW OBSOLETE FUNCTIONS *************************************
// **************************** BELOW OBSOLETE FUNCTIONS *************************************
// **************************** BELOW OBSOLETE FUNCTIONS *************************************
// **************************** BELOW OBSOLETE FUNCTIONS *************************************
// **************************** BELOW OBSOLETE FUNCTIONS *************************************

// util selection functions
//------------------------------------------------------------
// calculates velocity as a function of mass and momentum
Double_t fBeta(Double_t* x_val, Double_t* par)
{
    Double_t Momentum = TMath::Abs(x_val[0]);
    Double_t Mass     = par[0];
    Double_t Beta     = 0.0;
    Double_t poverm   = 0.0;

    if(Mass > 0.0) {
       poverm = Momentum/Mass;
       Beta   = poverm*1.0/(sqrt(poverm*poverm+1.0));
    }
    return Beta;
}

Bool_t isGoodRich(HParticleCand* cand){
    // return kTRUE if good rich
    //Float_t fRingPM  = cand->getRingPatternMatrix();
    //Float_t fRingNP  = cand->getRingNumPads();
    //Float_t fRingRC  = cand->getRingCentroid();
//    Float_t fRingAC = cand->getRingAmplitude()/cand->getRingNumPads();
//    if(fRingAC>45.){ return  kTRUE; } else return kFALSE;
    return kTRUE;
}



Bool_t myselectlepton(HParticleCand* pcand)
{

  // do your selection
  // must return kTRUE if selection criteria is fullfilled

  if(!pcand)                    return kFALSE;
  if (pcand->getSystem()<0)          return kFALSE;
  if (pcand->getMomentum()>5000) return kFALSE;
  if (pcand->getChi2()>1000.) return kFALSE;
  if (pcand->getBeta()<0.01)   return kFALSE;
  if (pcand->getInnerSegmentChi2()==-1)   return kFALSE;
  // contemporary conditions:
/*
  if (pcand->isFlagAND(5,
                       Particle::kIsAcceptedHitRICH,
                       Particle::kIsAcceptedHitInnerMDC,
                       Particle::kIsAcceptedHitOuterMDC,
                       Particle::kIsAcceptedHitMETA,
                       Particle::kIsAcceptedRK)) return kTRUE;
*/
  return kTRUE;
}

Bool_t myselecthadron(HParticleCand* pcand)
{

  // do your selection
  // must return kTRUE if selection criteria is fullfilled

  if(!pcand)                   return kFALSE;
  if (pcand->getSystem()<0)          return kFALSE;
  if (pcand->getMomentum()>5000) return kFALSE;
  if (pcand->getChi2()>1000.) return kFALSE;
  if (pcand->getBeta()<0.01)   return kFALSE;
  //if (pcand->getBeta(4)>2)   return kFALSE;
  if (pcand->getInnerSegmentChi2()==-1)   return kFALSE;
  // contemporary conditions:
/*
    if (pcand->isFlagAND(4,
                       Particle::kIsAcceptedHitInnerMDC,
                       Particle::kIsAcceptedHitOuterMDC,
                       Particle::kIsAcceptedHitMETA,
                       Particle::kIsAcceptedRK)) return kTRUE;

*/
  return kTRUE;
}


